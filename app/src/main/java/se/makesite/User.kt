package se.makesite

data class User (
    val name: String,
    val lastName: String,
    val email: String,
    val phone: String
)