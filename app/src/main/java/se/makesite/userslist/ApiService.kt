package se.makesite.userslist

import retrofit2.Call
import retrofit2.http.GET
import se.makesite.User

interface ApiService {

    @GET("/users")


fun fetchAllUser(): Call<List<User>>
}