package se.makesite.userslist

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.user_row.view.*
import se.makesite.User

class UsersAdapter(private val users: List<User>) : RecyclerView.Adapter<UsersAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.user_row,
            parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount() = users.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = users[position]

        holder.firstName.text = user.name
        holder.phone.text = user.phone


    }

    class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView){

        val firstName: TextView = itemView.firstName
        val phone: TextView = itemView.lastName



    }

}
